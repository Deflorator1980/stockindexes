package hell;


public class Indexes {
    private String Company;
    private String Price;
    private String Date;
    private String Time;

    public Indexes(String company, String price, String date, String time) {
        Company = company;
        Price = price;
        Date = date;
        Time = time;
    }

    public String getCompany() {
        return Company;
    }

    public String getPrice() {
        return Price;
    }

    public String getDate() {
        return Date;
    }

    public String getTime() {
        return Time;
    }
}
