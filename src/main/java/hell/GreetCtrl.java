package hell;

import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;

@RestController
public class GreetCtrl {

//    @CrossOrigin(origins = "http://localhost:8000")
    @RequestMapping("/show")
    public Indexes show(@RequestParam(value = "company") String company) {
        String csv;
        URL url;
        URLConnection urlCon;
        InputStreamReader inStream = null;
        BufferedReader buff = null;

        try {
            url = new URL("http://quote.yahoo.com/d/quotes.csv?s="
                    + company + "&f=sl1d1t1c1ohgv&e=.csv");
            urlCon = url.openConnection();
            inStream = new InputStreamReader(urlCon.getInputStream());
            buff = new BufferedReader(inStream);

            csv = buff.readLine();
            csv = csv.replace("\"", "");
            StringTokenizer tokenizer = new StringTokenizer(csv, ",");
            String ticker = tokenizer.nextToken();
            String price = tokenizer.nextToken();
            String tradeDate = tokenizer.nextToken();
            String tradeTime = tokenizer.nextToken();

            return new Indexes(ticker, price, tradeDate, tradeTime);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert inStream != null;
                inStream.close();
                assert buff != null;
                buff.close();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
            return new Indexes("", "", "", "");


    }
}
